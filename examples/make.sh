#!/bin/sh
#
# usage: ./make.sh PROGRAM FRONTEND
# e.g.:  ./make.sh minigames sdl

PROGRAM=$1
LINK=''
DEF=''

clear; clear;

if [ $2 = "sdl" ]; then
  DEF='-DSAF_PLATFORM_SDL2=1'
  LINK='-lSDL2'
elif [ $2 = "csfml" ]; then
  DEF='-DSAF_PLATFORM_CSFML=1'
  LINK='-lcsfml-graphics -lcsfml-window -lcsfml-system -lcsfml-audio' 
elif [ $2 = "x11" ]; then
  DEF='-DSAF_PLATFORM_X11=1'
  LINK='-lX11' 
elif [ $2 = "ncurses" ]; then
  DEF='-DSAF_PLATFORM_NCURSES=1'
  LINK='-lncurses' 
else
  echo "error: second parameter has to be frontend (sdl, csfml, x11, ...)"
  exit 1
fi

g++ -x c -g -std=c99 -O3 -fmax-errors=5 -pedantic -Wall -Wextra $DEF -lm -o ${PROGRAM} ${PROGRAM}.h $LINK && ./${PROGRAM}
